package example;

import java.net.MalformedURLException;
import java.net.URL;

import org.kie.services.client.api.RemoteRestRuntimeEngineFactory;
import org.kie.services.client.api.builder.RemoteRestRuntimeEngineFactoryBuilder;

public class Populator {

	private static URL URL;
	{
		try {
			URL = new URL("http://localhost:8080/business-central/");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			URL = null;
		}
	}
	private static final String USER = "bpmsAdmin";
	private static final String PASSWORD = "jbossadmin1!";
	private static final String DEPLOYMENT_ID_1 = "org.kie.example:project1:1.0";
	private static final String PROCESS_ID_1 = "project1.process";
	private static final String DEPLOYMENT_ID_2 = "org.kie.example:project2:1.0";
	private static final String PROCESS_ID_2 = "project2.process";

	public static void main(String[] args) throws MalformedURLException {
		Populator p = new Populator();
		p.startProcesses(DEPLOYMENT_ID_1, PROCESS_ID_1);
		p.startProcesses(DEPLOYMENT_ID_2, PROCESS_ID_2);

	}

	private void startProcesses(String deploymentId, String processId) {
		RemoteRestRuntimeEngineFactoryBuilder builder = RemoteRestRuntimeEngineFactory
				.newBuilder().addUrl(URL).addUserName(USER)
				.addPassword(PASSWORD).addDeploymentId(deploymentId);

		for (int i = 0; i < 500; i++)
			builder.build().newRuntimeEngine().getKieSession()
					.startProcess(processId);
	}
	
	
}
