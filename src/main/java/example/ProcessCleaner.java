package example;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jbpm.process.audit.ProcessInstanceLog;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.services.client.api.RemoteRestRuntimeEngineFactory;
import org.kie.services.client.api.builder.RemoteRestRuntimeEngineFactoryBuilder;
import org.kie.services.client.api.command.RemoteRuntimeEngine;

public class ProcessCleaner {

	private static final String URL = "http://localhost:8080/business-central/";
	private static final String USER = "bpmsAdmin";
	private static final String PASSWORD = "jbossadmin1!";
	private static final String DEPLOYMENT_ID = "org.kie.example:project1:1.0";
	private static final List<String> PROCESS_IDS = Arrays.asList(
			"project1.process", "project1.process2");

	public static void main(String[] args) throws MalformedURLException {

		URL url = new URL(URL);

		RemoteRestRuntimeEngineFactoryBuilder builder = RemoteRestRuntimeEngineFactory
				.newBuilder().addUrl(url).addUserName(USER)
				.addPassword(PASSWORD).addDeploymentId(DEPLOYMENT_ID);

		RemoteRuntimeEngine re = builder.build().newRuntimeEngine();

		// get all processes in the deployment
		List<ProcessInstanceLog> processes = new ArrayList<ProcessInstanceLog>();
		for (String processId : PROCESS_IDS) {
			processes.addAll(re.getAuditLogService().findProcessInstances(
					processId));
		}

		// end them if they are pending or active
		for (ProcessInstanceLog p : processes) {
			if (p.getStatus() < 2) {
				try {
					RuntimeEngine runtimeEngine = builder
							.addProcessInstanceId(p.getProcessInstanceId())
							.build().newRuntimeEngine();
					KieSession ksession = runtimeEngine.getKieSession();
					ksession.abortProcessInstance(p.getProcessInstanceId());
				} catch (Exception e) {
					System.out.println("Unable to abort process with id: "
							+ p.getProcessInstanceId());
					e.printStackTrace();
				}
			}
		}

	}

}
