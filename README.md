# Run Process Cleaner

Update URL, USER, PASSWORD, and DEPLOYMENT_ID to match environment you want to clean

Specify the process ids you want to clean in PROCESS_IDS

Execute ProcessCleaner.java

All processes in `PENDING` or `ACTIVE` state in given deployment with matching process ids will be aborted
